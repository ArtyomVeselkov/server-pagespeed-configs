Statistics and etc
==================

# What for
- Image optimization: stripping meta-data, dynamic resizing, recompression
- CSS & JavaScript minification, concatenation, inlining, and outlining
- Small resource inlining
- Deferring image and JavaScript loading
- HTML rewriting
- Cache lifetime extension
- and more

# Sources
- For Apache: https://github.com/pagespeed/mod_pagespeed
- For Nginx: https://github.com/pagespeed/ngx_pagespeed

# Short Descriptions
- [Wiki](https://en.wikipedia.org/wiki/Google_PageSpeed_Tools)