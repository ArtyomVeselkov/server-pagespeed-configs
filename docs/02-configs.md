Main Optimization Settings
==========================

# General
Modules' names:
  - For Apache: `ModPagespeedEnableFilters`
  - For Nginx: `pagespeed`

## Install
  - Detailed:
    + [CentOS](http://tantruminv.com/articles/installing-apache-2-4-6-mod_pagespeed-on-centos-6-4-64-bit/)


## HTTPS
- [Documentation](https://modpagespeed.com/doc/https_support#https_fetch)
1) Enable fetching (* - _please, pay here attention_):
    ```
    ModPagespeedFetchHttps enable
    ```

## CSS

### CSS Optimization

### Inline Fonts
- [Documentation](https://modpagespeed.com/doc/filter-css-inline-google-fonts)
1) Filter:
   ```
   ModPagespeedEnableFilters inline_google_font_css
   ```
   
   
# Use

## Install 
   - EasyApache (cPanel)
     - [Official](https://modpagespeed.com/doc/faq#cpanel-install)
   - Build from source
     - [Official](https://modpagespeed.com/doc/build_mod_pagespeed_from_source)

## Enable/Disable via GET requests
   - Disable with GET parameter [PageSpeed broke my site; what do I do?](https://modpagespeed.com/doc/faq#broken)
     ```
     ?ModPagespeed=off
     ```
   - Debug
     ```
     ?PageSpeedFilters=+debug
     ```
   - Check if PS is working [https://ismodpagespeedworking.com/](https://ismodpagespeedworking.com/)
        
## Configs
   - .htaccess - enable console
   ```
   RewriteCond %{REQUEST_FILENAME} !/pagespeed_admin #disable rewrite for url
   ```
   - disable for admin panel
   ```
   ModPagespeedDisallow "*index.php/admin/*"
   ```


## Possible issues over net
   - [Segmentation fault with latest mod-pagespeed-beta](https://github.com/pagespeed/mod_pagespeed/issues/1490)
   
   
## Interesting suggestions
   - [http://stackoverflow.com/questions/4105441/mod-pagespeed-magento](http://stackoverflow.com/questions/4105441/mod-pagespeed-magento)
   - [https://www.h-o.nl/blog/best-magento-mod_pagespeed-configuration](https://www.h-o.nl/blog/best-magento-mod_pagespeed-configuration)